# multi-tinyproxy

This is a binary build of a tinyproxy fork adding support for
multi-client rules. The reason for this fork is to replace a
badly-performant squid installation used without its caching
support.

https://gitlab.com/pgregoire-ci/multi-tinyproxy/-/jobs/artifacts/main/raw/tinyproxy.tar.gz?job=build

## References

https://tinyproxy.github.io/
